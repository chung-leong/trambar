Main Program
------------
The central code of the Trambar client application. This is where interactions
between different components are coordinated.

[icon]: fa://fa-home
