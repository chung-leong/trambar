Bootstrap Code
--------------
Code that handles initiation of the application.

```match
bootstrap.js
libraries.js
```

[icon]: fa://fa-rocket
