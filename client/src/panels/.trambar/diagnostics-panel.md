Diagnostics Panel
-----------------
Panel in Settings page that shows diagnostic information. Hidden normally.

```match
diagnostics-panel.*
diagnostic-data-panel.*
```
