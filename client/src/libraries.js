module.exports = {
    'bluebird': () => import('bluebird' /* webpackChunkName: "bluebird" */),
    'chartist': () => import('chartist' /* webpackChunkName: "chartist" */),
    'diff': () => import('diff' /* webpackChunkName: "diff" */),
    'font-awesome-webpack': () => import('font-awesome-webpack' /* webpackChunkName: "font-awesome-webpack" */),
    'hammerjs': () => import('hammerjs' /* webpackChunkName: "hammerjs" */),
    'jsmediatags': () => import('jsmediatags/dist/jsmediatags' /* webpackChunkName: "jsmediatags" */),
    'lodash': () => import('lodash' /* webpackChunkName: "lodash" */),
    'mark-gor': () => import('mark-gor' /* webpackChunkName: "mark-gor" */),
    'moment': () => import('moment' /* webpackChunkName: "moment" */),
    'qrcode': () => import('qrcode' /* webpackChunkName: "qrcode" */),
    'sockjs-client': () => import('sockjs-client' /* webpackChunkName: "sockjs-client" */),
    'react': () => import('react' /* webpackChunkName: "react" */),
    'react-dom': () => import('react-dom' /* webpackChunkName: "react-dom" */),
};
