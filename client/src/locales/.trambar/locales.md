Client Localization Packages
----------------------------
Support for different languages in the end-user application.

```match
*.js
```

[icon]: fa://fa-language/#ffe77c
