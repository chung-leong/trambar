module.exports = exports = [
    'like',
    'comment',
    'vote',
    'task-completion',
    'note',
    'assignment',
    'tracking',
];

exports.editable = [
    'comment',
];

exports.git = [
    'note',
    'assignment',
];
