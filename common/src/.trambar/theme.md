Theme Management
----------------
Code that manages the application's appearance based on screen resolution.

```match
theme/*
!test/
```
