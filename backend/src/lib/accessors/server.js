var _ = require('lodash');
var Promise = require('bluebird');
var Data = require('accessors/data');

module.exports = _.create(Data, {
    schema: 'global',
    table: 'server',
    columns: {
        id: Number,
        gn: Number,
        deleted: Boolean,
        ctime: String,
        mtime: String,
        details: Object,
        type: String,
        name: String,
        disabled: Boolean,
        settings: Object,
    },
    criteria: {
        id: Number,
        deleted: Boolean,
        type: String,
        name: String,
        disabled: Boolean,
    },

    /**
     * Create table in schema
     *
     * @param  {Database} db
     * @param  {String} schema
     *
     * @return {Promise<Result>}
     */
    create: function(db, schema) {
        var table = this.getTableName(schema);
        var sql = `
            CREATE TABLE ${table} (
                id serial,
                gn int NOT NULL DEFAULT 1,
                deleted boolean NOT NULL DEFAULT false,
                ctime timestamp NOT NULL DEFAULT NOW(),
                mtime timestamp NOT NULL DEFAULT NOW(),
                details jsonb NOT NULL DEFAULT '{}',
                name varchar(128) NOT NULL,
                type varchar(64),
                disabled boolean NOT NULL DEFAULT false,
                settings jsonb NOT NULL DEFAULT '{}',
                PRIMARY KEY (id)
            );
            CREATE UNIQUE INDEX ON ${table} (name) WHERE deleted = false;
        `;
        return db.execute(sql);
    },

    /**
     * Grant privileges to table to appropriate Postgres users
     *
     * @param  {Database} db
     * @param  {String} schema
     *
     * @return {Promise<Boolean>}
     */
    grant: function(db, schema) {
        var table = this.getTableName(schema);
        // Auth Manager needs to be able to update a server's OAuth tokens
        var sql = `
            GRANT SELECT, UPDATE ON ${table} TO auth_role;
            GRANT INSERT, SELECT, UPDATE ON ${table} TO admin_role;
        `;
        return db.execute(sql).return(true);
    },

    /**
     * Attach triggers to the table.
     *
     * @param  {Database} db
     * @param  {String} schema
     *
     * @return {Promise<Boolean>}
     */
    watch: function(db, schema) {
        return this.createChangeTrigger(db, schema).then(() => {
            var propNames = [ 'deleted', 'disabled', 'type' ];
            return this.createNotificationTriggers(db, schema, propNames).then(() => {
                // completion of tasks will automatically update details->resources
                var Task = require('accessors/task');
                return Task.createUpdateTrigger(db, schema, 'updateServer', 'updateResource', [ this.table ]);
            });
        });
    },

    /**
     * Export database row to client-side code, omitting sensitive or
     * unnecessary information
     *
     * @param  {Database} db
     * @param  {String} schema
     * @param  {Array<Object>} rows
     * @param  {Object} credentials
     * @param  {Object} options
     *
     * @return {Promise<Object>}
     */
    export: function(db, schema, rows, credentials, options) {
        return Data.export.call(this, db, schema, rows, credentials, options).then((objects) => {
            _.each(objects, (object, index) => {
                var row = rows[index];
                object.type = row.type;
                object.name = row.name;
                if (credentials.unrestricted) {
                    object.settings = _.obscure(row.settings, sensitiveSettings);
                    object.disabled = row.disabled;
                } else {
                    if (row.disabled) {
                        object.disabled = row.disabled;
                    }
                }
            });
            return objects;
        });
    },

    /**
     * Import objects sent by client-side code, applying access control
     *
     * @param  {Database} db
     * @param  {String} schema
     * @param  {Array<Object>} objects
     * @param  {Array<Object>} originals
     * @param  {Object} credentials
     * @param  {Object} options
     *
     * @return {Promise<Array>}
     */
    import: function(db, schema, objects, originals, credentials, options) {
        return Data.import.call(this, db, schema, objects, originals, credentials, options).mapSeries((serverReceived, index) => {
            var serverBefore = originals[index];
            if (serverReceived.settings instanceof Object) {
                _.each(sensitiveSettings, (path) => {
                    // restore the original values if these fields are all x's
                    var value = _.get(serverReceived.settings, path);
                    if (/^x+$/.test(value)) {
                        var originalValue = _.get(serverBefore.settings, path);
                        _.set(serverReceived.settings, path, originalValue);
                    }
                });
            }
            return this.ensureUniqueName(db, schema, serverBefore, serverReceived);
        });
    },

    /**
     * Create associations between newly created or modified rows with
     * rows in other tables
     *
     * @param  {Database} db
     * @param  {String} schema
     * @param  {Array<Object>} objects
     * @param  {Array<Object>} originals
     * @param  {Array<Object>} rows
     * @param  {Object} credentials
     *
     * @return {Promise}
     */
     associate: function(db, schema, objects, originals, rows, credentials) {
         return Promise.try(() => {
             var deletedServers = _.filter(rows, (serverAfter, index) => {
                 var serverBefore = originals[index];
                     return serverAfter.deleted && !serverBefore.deleted;
                     if (serverBefore) {
                 }
             });
             var undeletedServers = _.filter(rows, (serverAfter, index) => {
                 var serverBefore = originals[index];
                 if (serverBefore) {
                     return !serverAfter.deleted && serverBefore.deleted;
                 }
             });
             var Repo = require('accessors/repo');
             return Promise.all([
                 Repo.deleteAssociated(db, schema, { server: deletedServers }),
                 Repo.restoreAssociated(db, schema, { server: undeletedServers }),
             ]);
         });
     },

    /**
     * See if a database change event is relevant to a given user
     *
     * @param  {Object} event
     * @param  {User} user
     * @param  {Subscription} subscription
     *
     * @return {Boolean}
     */
    isRelevantTo: function(event, user, subscription) {
        if (Data.isRelevantTo.call(this, event, user, subscription)) {
            // not used in client app
            if (subscription.area === 'admin') {
                return true;
            }
        }
        return false;
    },
});

var sensitiveSettings = [
    'api.access_token',
    'api.refresh_token',
    'oauth.client_secret',
];
