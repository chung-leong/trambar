var _ = require('lodash');
var Promise = require('bluebird');
var Phantom = require('phantom');
var Moment = require('moment');

module.exports = {
    createScreenshot,
};

/**
 * Make screencap of website, returning the document title
 *
 * @param  {String} url
 * @param  {String} dstPath
 * @param  {Object} options
 *
 * @return {Promise<String>}
 */
function createScreenshot(url, dstPath, options) {
    var width = _.get(options, 'width', 1024);
    var height = _.get(options, 'height', 1024);
    var onProgress = _.get(options, 'onProgress');
    var reportProgress = (progress) => {
        if (onProgress) {
            onProgress(progress);
        }
    };
    return startPhantom().then((instance) => {
        return B(instance.createPage()).then((page) => {
            return B(page.setting('userAgent')).then((ua) => {
                // indicate in the UA string that this is a bot
                var settings = {
                    userAgent: ua + ' (compatible; trambarbot/1.0; +http://www.trambar.io/bot.html)',
                };
                return Promise.each(_.keys(settings), (key) => {
                    return page.setting(key, settings[key]);
                });
            }).then(() => {
                var properties = {
                    viewportSize: { width,  height },
                    clipRect: { left: 0, top: 0, width, height },
                };
                return Promise.each(_.keys(properties), (key) => {
                    return page.property(key, properties[key]);
                })
            }).then(() => {
                return page.open(url);
            }).then(() => {
                // say it's 50 percent done when we got the page
                reportProgress(50);

                var start = new Date;
                var last = start;
                var count = 0;
                page.on("onResourceRequested", (requestData) => {
                    // mark the time when a file request occurs
                    last = new Date;

                    // report some number just to show something is happening
                    count++;
                    if (count === 20) {
                        reportProgress(70);
                    } else if (count === 50) {
                        reportProgress(80);
                    }
                });
                return new Promise((resolve, reject) => {
                    var interval = setInterval(() => {
                        // we're done when there's a half a second pause in
                        // loading or after five seconds
                        var now = new Date;
                        var progress1 = (now - last) / 500;
                        var progress2 = (now - start) / 5000;
                        if (progress1 > 1 || progress2 > 1) {
                            clearInterval(interval);
                            resolve();
                        }
                    }, 100);
                });
            }).then(() => {
                return page.render(dstPath);
            }).then(() => {
                return page.invokeMethod('evaluate', function() {
                    return document.title;
                });
            });
        });
    });
}

var phantomPromise;

function startPhantom() {
    if (!phantomPromise) {
        phantomPromise = B(Phantom.create(['--ignore-ssl-errors=yes']));
    }
    return phantomPromise;
}

function shutdownPhantom() {
    if (phantomPromise) {
        phantomPromise.then((instance) => {
            instance.exit();
        });
        phantomPromise = null;
    }
}

/**
 * Convert promise to a Bluebird promise
 *
 * @param {Promise} promise
 */
function B(promise) {
    return Promise.resolve(promise);
}

process.on('beforeExit', () => {
    shutdownPhantom();
});
