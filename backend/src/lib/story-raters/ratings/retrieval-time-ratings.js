module.exports = {
    'recent': 50,
    'today': 40,
    'yesterday': 30,
    'week': 20,
    'month': 10,
    'year': 5,
};
