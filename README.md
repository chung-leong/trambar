Trambar
-------

Trambar is a companion tool to GitLab. It lets you transform your GitLab
activity log into a Facebook-style news feed, making the information more
accessible to those without a programming background. It provides greater
transparency to your software development process, with the aim of fostering
trust between programmers and other stakeholders.

* [Features](#features)
* [Demo](#demo)
* [Getting started](docs/getting-started.md)
* [User guide](docs/user-guide.md)
* [User guide - Administrative Console](docs/user-guide-admin.md)
* [Source tree decoration](docs/decoration.md)
* [System architecture](docs/architecture.md)
* [Acknowledgments](docs/acknowledgments.md)

## Demo

Visit the Trambar project's live site at https://live.trambar.io/. You can sign
in using either your GitHub or Facebook account.

## Features

* Real-time updates as changes occurs
* Monitoring of multiple repositories
* [Source-tree annotation](docs/decoration.md)
* Accessible to external users through Facebook and other OAuth providers
* Exporting videos from mobile phone into GitLab's issue tracker
* Responsive user-interface
* Mobile notification
* App for Android, iOS, and Windows Phone
* Designed with device loss in mind
* Designed to leave you in control of your own data
* Completely open-source

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file
for details
